---
layout: page
title: About
permalink: /about/
navigation_weight: 2
---
# Blog GDG Toledo PR

Este é um blog colaborativo gerado através do jekyll. Você pode contribuir, com artigos, sugestões, correções ou críticas através do nosso [repositório](https://gitlab.com/gdg-toledo-pr/gdg-toledo-pr.gitlab.io) no gitlab.

Link do tema utilizado: [https://github.com/jaehee0113/console](https://github.com/jaehee0113/console)

 * [Facebook](https://www.facebook.com/groups/cakebrasil/)
 * [Slack](http://slack.gdgtoledorpr.com.br/) Não tem cadastro? Solicite seu [convite](http://gdgtoledopr.herokuapp.com)
 * [Grupo do Whatsapp](https://chat.whatsapp.com/0xnRJVwNZ3BLCdLfC9n0x9)
 * [Meetup](https://www.meetup.com/pt-BR/GDG-Toledo-PR/)
