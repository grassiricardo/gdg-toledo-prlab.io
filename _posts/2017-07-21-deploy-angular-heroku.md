---
layout: post
title:  "Publicando uma aplicação Angular no Heroku"
date:   2017-07-26 22:00:00
categories: angular angular-cli heroku express
author: 'Ricardo Grassi'
author_url: 'https://github.com/grassiricardo'
author_url_label: 'Ricardo Grassi'
author_url_avatar: 'https://avatars1.githubusercontent.com/u/4211878?v=4&u=6ebecc815c89e46ac1af0b2596010ab6be792c6b&s=400'
disqus_identifier: a815331eadf93f8f
comments: true
highlights: false
---

Neste artigo vamos ver como realizar o deploy de uma aplicação Angular no [Heroku](https://imasters.com.br/box/ferramenta/heroku/), servidor muito popular para linguagens como Ruby on Rails, PHP, Node, onde um dos grandes fatores de seu uso é o plano free que apesar de ser free contem um grande suporte e serve muito bem para aplicações que estão em desenvolvimento ainda. Vou mostrar o quanto simples é fazer o deploy no Heroku, tenho certeza que será de grande vália para todos. Bora lá?

## Para este artigo estou usando a ultima versão do Angular (Versão 4) ##

## Pré requisitos ##
1. Windows, Mac ou Linux
2. [Node] (https://nodejs.org/en/)
3. [Angular CLI] (https://cli.angular.io/)
4. [Heroku](https://www.heroku.com/)

## Criando a aplicação no Heroku ##
